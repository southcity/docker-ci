module.exports = {
  plugins: [require('@tailwindcss/ui')],
  purge: ['./pages/**/*.{js,ts,jsx,tsx}'],
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
}

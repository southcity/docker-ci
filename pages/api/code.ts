import { spawnSync } from 'child_process'
import { NextApiHandler } from 'next'

export type CodeHandlerData = {
  stdout: string
  stderr: string
  status: number
}

const CodeHandler: NextApiHandler<CodeHandlerData> = (req, res) => {
  const { lang, code } = req.body

  const cmd = spawnSync('docker', ['run', '--rm', lang, '-e', code], {
    stdio: 'pipe',
  })

  const data: CodeHandlerData = {
    stdout: cmd.stdout.toString(),
    stderr: cmd.stderr.toString(),
    status: cmd.status,
  }

  res.status(200).json(data)
}

export default CodeHandler

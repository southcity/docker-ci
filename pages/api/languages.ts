import { NextApiHandler } from 'next'
import { languages } from '../../lib/constants'

export type LanguageHandlerData = {
  languages: string[]
}

const LanguageHandler: NextApiHandler<LanguageHandlerData> = (_, res) => {
  res.status(200).json({ languages })
}

export default LanguageHandler

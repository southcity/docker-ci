import { NextPage } from 'next'
import { FormEvent, useState } from 'react'
import { languages } from '../lib/constants'

export type IndexPageProps = {}

const IndexPage: NextPage<IndexPageProps> = () => {
  const [lang, setLang] = useState(languages[0])
  const [code, setCode] = useState('')
  const [result, setResult] = useState({ stdout: '', stderr: '', status: -1 })
  const [loading, setLoading] = useState(false)

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()

    setLoading(true)

    const res = await fetch('/api/code', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ lang, code }),
    })

    const data = await res.json()

    setLoading(false)
    setResult(data)
  }

  return (
    <main className="container mx-auto my-16 prose">
      <h1>Code Runner</h1>
      <form onSubmit={handleSubmit} className="my-8">
        <div className="flex flex-col my-8">
          <label htmlFor="lang">Language:</label>
          <select
            name="lang"
            id="lang"
            value={lang}
            onChange={(event) => setLang(event.currentTarget.value)}
            className="bg-white focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
          >
            {languages.map((lang) => (
              <option key={lang} value={lang}>
                {lang}
              </option>
            ))}
          </select>
        </div>
        <div className="flex flex-col my-8">
          <label htmlFor="code">Code:</label>
          <textarea
            name="code"
            id="code"
            cols={70}
            rows={10}
            value={code}
            onChange={(event) => setCode(event.currentTarget.value)}
            className="bg-white focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
          />
        </div>
        <input
          type="submit"
          value={loading ? 'Running...' : 'Run'}
          disabled={loading}
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
        />
      </form>
      <section className="my-8">
        <article>
          <h3>Standard output</h3>
          <textarea
            cols={2}
            readOnly
            value={result.stdout}
            className="bg-white focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
          />
        </article>
        <article>
          <h3>Standard error</h3>
          <textarea
            cols={2}
            readOnly
            value={result.stderr}
            className="bg-white focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
          />
        </article>
        <article>
          <h3>Exit status</h3>
          <textarea
            cols={2}
            readOnly
            value={result.status}
            className="bg-white focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
          />
        </article>
      </section>
    </main>
  )
}

export default IndexPage
